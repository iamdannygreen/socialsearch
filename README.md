# README #



### Introduction ###

Social Search is a PHP CLI which allows the user to search for users via the Facebook & Twitter APIs.

### Usage ###

Social Search can be run by changing working directory to the containing folder and running `php socialsearch.php` via the command line.

Optional Parameters

The CLI can be run without passing any parameters, in which case the user will be prompted for their selected platform and search query. Alternatively you can pass this as part of the initial command. ie. `php socialsearch.php -p "Twitter" -q "Jeremy Clarkson"`.

--help (-h) 		View Help File (You're already reading it)
--platform (-p) 	Social Media platform you would like to query [Facebook/Twitter]
--query (-q) 		The query string to be passed to the API

### Installing from the Git Repository ###

If you have downloaded Social Search via the Git Repository then you will can use Composer to install the dependencies. Instructions for installing Composer on your machine can be found here [https://getcomposer.org/doc/00-intro.md].

Once installed, you can run composer on the application directory by running `composer install`.

### Dependencies ###

* Getopt [https://github.com/ulrichsg/getopt-php]
* TwitterOAuth [https://github.com/abraham/twitteroauth]
* Facebook Graph SDK [https://developers.facebook.com/docs/reference/php/]

### Why should Creode hire me? ###

I'm a passionate Web Developer with experience managing teams and leading client meetings. I'm proudly self-taught and for the past 2 years have focused on building PHP applications using the Laravel MVC Framework.

I enjoy working in vibrant, fast paced office environments and strive to work with other members of my team in their personal development.

© Danny Green 2017