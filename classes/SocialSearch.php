<?php

class SocialSearch
{
	private $platform;
	private $query;
	private $searchResults = array();
	private $selectedUser;

	public function setPlatform($platform)
	{
		$this->platform = ucwords(trim($platform));
	}

	public function getPlatform()
	{
		return $this->platform;
	}

	public function setQuery($query)
	{
		$this->query = trim($query);
	}

	public function getQuery()
	{
		return $this->query;
	}

	public function getSearchResults()
	{
		return $this->searchResults;
	}

	public function runSearch()
	{
		if ($this->platform === "Facebook") {

			self::facebookSearch();	

		} else if ($this->platform === "Twitter") {

			self::twitterSearch();

		} else {
			exit("The platform" . $this->platform . " is not supported.");
		}
	}

	private function facebookSearch()
	{
		global $fb;
			
		$request = $fb->request('GET', '/search?q='. $this->query .'&type=User');

		try {
			$response = $fb->getClient()->sendRequest($request);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			echo 'Graph returned an error: ' . $e->getMessage();
		exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
		}

		$results = $response->getGraphEdge();

		$this->searchResults = $results;
	}

	private function twitterSearch()
	{
		global $twitter;

		$results = $twitter->get("users/search", ["q" => $this->query, "count" => 10]);
		$this->searchResults = $results;
	}

	public function getSelectedUser()
	{
		return $this->selectedUser;
	}

	public function setSelectedUser($id)
	{
		$this->selectedUser = new SelectedUser($this->searchResults[$id]);
	}
}

?>