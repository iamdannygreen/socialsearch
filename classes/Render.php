<?php

class Render
{
	public static function displaySearchResults($search)
	{
		system('clear');
		if ($search->getPlatform() === "Facebook") {
			self::displayFacebookSearchResults($search->getSearchResults());
		}
		if ($search->getPlatform() === "Twitter") {
			self::displayTwitterSearchResults($search->getSearchResults());
		}
		self::displaySearchResultsNavigation($search);
	}

	public static function displayFacebookSearchResults($results)
	{
		print("\n\e[34m---------- Facebook User Search Results ---------- \033[0m\n\n");

		$count = 0;

		foreach ($results as $user) {
			$count++;
			print($count . ". " . $user['name'] . " (" . $user['id'] . ")\n");
		}

		print("\n\e[34m----------     End of Search Results    ----------\033[0m\n\n");
	}

	public static function displayTwitterSearchResults($results)
	{
		print("\n\e[36m---------- Twitter User Search Results ---------- \033[0m\n\n");

		$count = 0;

		foreach ($results as $user) {
			$count++;
			print($count . ". " . $user->name . " (@" . $user->screen_name . ") " . $user->location .  "\n");
		}

		print("\n\e[36m----------     End of Search Results    ----------\033[0m\n\n");
	}

	private static function displayUserProfile($search)
	{
		system('clear');
		if ($search->getPlatform() === "Facebook") {
			self::displayFacebookProfile($search->getSelectedUser());
		}
		if ($search->getPlatform() === "Twitter") {
			self::displayTwitterProfile($search->getSelectedUser());
		}
		self::displayUserProfileNavigation($search);
	}

	private static function displayFacebookProfile($user)
	{
		print("\n\n\e[34m---------- " . $user->getUserProfile()['name'] . " ---------- \033[0m \n\n");

		print('Full Name: ' . $user->getUserProfile()['name'] . "\n");
		print('Facebook User ID: ' . $user->getUserProfile()['id'] . "\n");

		print("\nRecent Posts aren't available for Facebook Profiles.\n\n");

		print("\n\e[34m-------------------------------------\033[0m\n\n");
	}

	private static function displayTwitterProfile($user)
	{
		print("\n\n\e[36m---------- " . "@" . $user->getUserProfile()->screen_name . " ---------- \033[0m \n\n");

		print('Full Name: ' . $user->getUserProfile()->name . "\n");
		print('Location: ' . $user->getUserProfile()->location . "\n");

		self::displayTwitterRecentPosts($user->getTwitterRecentPosts());

		print("\n\e[36m-------------------------------------\033[0m\n\n");
	}

	private static function displayTwitterRecentPosts($posts)
	{
		print("\nRECENT POSTS\n\n");

		foreach ($posts as $post) {
			print($post->created_at . "\n");
			print($post->text . "\n");
			print("---\n\n");
		}
	}

	private static function displaySearchResultsNavigation($search)
	{
		echo "[0] To Exit | [Number of User] To View Profile: ";
		$answer = trim(fgets(STDIN, 1024));
		
		if ($answer === "0") {
			exit;
		} else {
			$search->setSelectedUser($answer - 1);
			self::displayUserProfile($search);
		}
	}

	private static function displayUserProfileNavigation($search)
	{
		echo "[1] Back to Search Results | [2] Exit: ";
		$answer = trim(fgets(STDIN, 1024));
		
		if ($answer === "2") {
			exit;
		} else {
			self::displaySearchResults($search);
		}
	}
}

?>