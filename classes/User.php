<?php


class SelectedUser {
	private $userDetails;

	public function __construct($user)
	{
		$this->userDetails = $user;
	}

	public function setUser($user)
	{
		$this->userDetails = $user;
	}

	public function getUserProfile()
	{
		return $this->userDetails;
	}

	public function getTwitterRecentPosts()
	{
		global $twitter;

		$posts = $twitter->get("statuses/user_timeline", ["user_id" => $this->userDetails->id, "count" => 3]);

		return $posts;
	}
}

	

?>