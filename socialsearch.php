<?php
	require('vendor/autoload.php');
	require('connections.php');
	require('functions.php');
	require('classes/SocialSearch.php');
	require('classes/Render.php');
	require('classes/User.php');
	use Ulrichsg\Getopt\Getopt;
	use Ulrichsg\Getopt\Option;
	use Abraham\TwitterOAuth\TwitterOAuth;

	$getopt = new Getopt(array(
	    new Option('h', 'help', Getopt::OPTIONAL_ARGUMENT),
	    new Option('p', 'platform', Getopt::OPTIONAL_ARGUMENT),
	    new Option('q', 'query', Getopt::OPTIONAL_ARGUMENT),
	));

	$getopt->parse();
	$options = $getopt->getOptions();
	
	if ($options['help'] === 1) { // Check if the user has used the --help (-h) option
		print(file_get_contents( "readme.md" ));
		print("\n\n");
	}

	while(true) {

		$search = new SocialSearch();

		if (isset($options['platform'])) {
			$search->setPlatform($options['platform']);
		} else {
			echo "Which platform would you like to search? [Facebook/Twitter]: ";
			$search->setPlatform(fgets(STDIN, 1024));
		}

		if (isset($options['query'])) {
			$search->setQuery($options['query']);
		} else {
			echo "Enter Search Query: ";
			$search->setQuery(fgets(STDIN, 1024));
		}

		$search->runSearch();

		Render::displaySearchResults($search);

	}

	
?>